#include "Util.h"
#include "enum_tipo_funcionario.h"

class FuncionarioMensalista : public Funcionario {
private:
	EnumTipoFuncionario tipo_funcionario;
public:

	virtual void set_tipo_funcionario(EnumTipoFuncionario tp_funcionario) {
		FuncionarioMensalista::tipo_funcionario = tp_funcionario;
	}

	virtual EnumTipoFuncionario get_tp_funcionario() {
		return FuncionarioMensalista::tipo_funcionario;
	}

	// metodo sobrescrito da classe Funcionario
	double get_salario() {
		if (FuncionarioMensalista::tipo_funcionario == EnumTipoFuncionario::PROGRAMADOR)
			return 4999.99;
		else if (FuncionarioMensalista::tipo_funcionario == EnumTipoFuncionario::ANALISTA)
			return 6999.99;
		else
			return 9999.99;
	}
};